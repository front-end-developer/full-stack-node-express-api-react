// obtain API key from your Skyscanner contact
const colors = require('./configEnvironment/colours');

// I can set this another way via command but this is for now...
process.env.APIKEY = 'ss630745725358065467897349852985';
const APIKEY = process.env.APIKEY;

console.log('APIKEY:' + APIKEY.silly);

if (!APIKEY) {
  console.error('APIKEY environment variable missing. Please re-run with `APIKEY=<key> npm run server`');
  process.exit(1);
}

module.exports = {
  apiKey: process.env.APIKEY,
  skyscannerApi: 'http://partners.api.skyscanner.net/'
};
