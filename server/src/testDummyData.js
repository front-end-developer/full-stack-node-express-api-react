/**
 * Created by WebEntra on 19/04/2018.
 */
const searchResults = require('./dummyReturnData');


// flightSummary
const itinerariesSummary = {
    price:  searchResults.Itineraries[0].PricingOptions[0].Price, // [two decimal number]
    agent:  searchResults.Itineraries[0].PricingOptions[0].Agents[0], // 		[array]
    link:   searchResults.Itineraries[0].PricingOptions[0].DeeplinkUrl, //	[String]
    outboundLegId:  searchResults.Itineraries[0].OutboundLegId,
    inboundLegId:   searchResults.Itineraries[0].InboundLegId
}
/*
const inboundLegs = searchResults.Legs.filter((leg) => {
    return leg.Id === itinerariesSummary.inboundLegId;
});

const outboundLegs = searchResults.Legs.filter((leg) => {
    return leg.Id === itinerariesSummary.outboundLegId;
});

const outBoundCarrier = searchResults.Carriers.filter((carrier) => {
    return carrier.Id === outboundLegs.Carriers[0];
});

const inBoundCarrier = searchResults.Carriers.filter((carrier) => {
    return carrier.Id === inboundLegs.Carriers[0];
});

const inboundSegment = searchResults.Segments.filter((segment) => {
    return (segment.FlightNumber === inboundLegs.FlightNumbers[0].FlightNumber && segment.Carrier === inboundLegs.FlightNumbers[0].CarrierId)
});

const outboundSegment = searchResults.Segments.filter((segment) => {
    return (segment.FlightNumber === outboundLegs.FlightNumbers[0].FlightNumber && segment.Carrier === outboundLegs.FlightNumbers[0].CarrierId)
});
*/




const inboundLegs = results.Legs.filter((leg) => {
    return leg.Id === itinerariesSummary.inboundLegId;
});

const outboundLegs = results.Legs.filter((leg) => {
    return leg.Id === itinerariesSummary.outboundLegId;
});

/**
 * @description get the Aircraft carrier information based on
 * the first ID inside the inboundlegs & outboundlegs
 * carrier array (use the first carrier Id in each array only, I THINK),
 * TODO: and then add it to each
 */
// TODO: use a better O notation algorithm
for (let item in outboundLegs) {
    const carrier = results.Carriers.filter(function (carrier) {
        console.log(outboundLegs[item].Carriers[0], '===', carrier.Id);
        return carrier.Id === outboundLegs[item].Carriers[0];
    });
    outboundLegs[item].CarriersInformation = carrier;
}

// TODO: use a better O notation algorithm
for (let item in inboundLegs) {
    const carrier = results.Carriers.filter(function (carrier) {
        console.log(inboundLegs[item].Carriers[0], '===', carrier.Id);
        return carrier.Id === inboundLegs[item].Carriers[0];
    });
    inboundLegs[item].CarriersInformation = carrier;
}

// what am I doing up to here, process of elimination

/**
 * TODO: perhaps I should not use only the first item in the
 * inboundLegs but I can fix it after the test
 *
 * @description not all Segments are marked as Outbound, so this may not be correct but, but Outbound might be standard for all inbound and outbound flights for Segments
 */
for (let item in inboundLegs) {
    const inboundSegment = results.Segments.find((segment) => {
        if (segment.FlightNumber === inboundLegs[item].FlightNumbers[0].FlightNumber
            && segment.Carrier === inboundLegs[item].FlightNumbers[0].CarrierId
            && segment.Id === inboundLegs[item].SegmentIds[0] // <-- TODO: verify why do we have an array of segment IDs here....
        ) {
            debugger;
            console.log('FOUND');
            console.log('1: ', inboundLegs[item].FlightNumbers[0].FlightNumber, '===', segment.FlightNumber);
            console.log('2: ', inboundLegs[item].FlightNumbers[0].CarrierId, '===', segment.Carrier);
            return segment;
        }
        // perhaps I should do something to send back {}
    });
    console.log('found: ', inboundSegment);
    delete inboundSegment.Directionality;
    inboundLegs[item].SegmentInformation = inboundSegment;
}

for (let item in outboundLegs) {
    const outboundSegment = results.Segments.find((segment) => {
        if (segment.FlightNumber === outboundLegs[item].FlightNumbers[0].FlightNumber
            && segment.Carrier === outboundLegs[item].FlightNumbers[0].CarrierId
            && segment.Id === outboundLegs[item].SegmentIds[0] // <-- TODO: verify why do we have an array of segment IDs here....
        ) {
            debugger;
            console.log('FOUND');
            console.log('1: ', outboundLegs[item].FlightNumbers[0].FlightNumber, '===', segment.FlightNumber);
            console.log('2: ', outboundLegs[item].FlightNumbers[0].CarrierId, '===', segment.Carrier);
            return segment;
        }
        // perhaps I should do something to send back {}
    });
    console.log('found: ', outboundSegment);
    delete outboundSegment.Directionality;
    outboundLegs[item].SegmentInformation = outboundSegment;
}

inboundLegs.forEach((leg) => {
    const DestinationStationPlaceCode = results.Places.filter((place) => {
        return place.Id === leg.DestinationStation;
    });
    leg.DestinationStationPlaceCode = DestinationStationPlaceCode;
    const OriginStationPlaceCode = results.Places.filter((place) => {
        return place.Id === leg.OriginStation;
    });
    leg.OriginStationPlaceCode = OriginStationPlaceCode;
});

outboundLegs.forEach((leg) => {
    const DestinationStationPlaceCode = results.Places.filter((place) => {
        return place.Id === leg.DestinationStation;
    });
    leg.DestinationStationPlaceCode = DestinationStationPlaceCode;
    const OriginStationPlaceCode = results.Places.filter((place) => {
        return place.Id === leg.OriginStation;
    });
    leg.OriginStationPlaceCode = OriginStationPlaceCode;
});


itinerariesSummary.outboundLegs = outboundLegs;
itinerariesSummary.inboundLegs = inboundLegs;
console.log('Object-itinerariesSummary:', JSON.stringify(itinerariesSummary, null, 7));
