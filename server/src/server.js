require('isomorphic-fetch');
require('es6-promise').polyfill();
const bodyParser = require('body-parser');
const express = require('express');
const app = express();
const api = require('./api/');
const colors = require('./configEnvironment/colours');

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(express.static('client'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.send('Hello World!');
});

/**
  Simple flight search api wrapper.

  TODO: client should provide params

  Api params and location values are here:
  http://business.skyscanner.net/portal/en-GB/Documentation/FlightsLivePricingQuickStart
*/
app.get('/api/search', (req, res) => {
    // TODO:
    // data validation and filtering
    /*
  console.log('server Request'.yellow);
  console.log('server Request adults: ', req.params);
  console.log('server Request class: ', req.params.class);
  console.log('server Request class: ', req.params.class);
  console.log('server Request toPlace: ', req.params.toPlace);
  console.log('server Request toDate: ', req.params.toDate);
  console.log('server Request fromPlace: ', req.params.fromPlace);
  console.log('server Request fromDate: ', req.params.fromDate);
  */

    // ADDED PARAMS FOR TESTING AND TO STYLE THE FRONT END QUICKLY
  api.livePricing.search({
    // TODO client to provide params
    // check in api docs what client should provide
      adults: 1,
      class: 'Economy',
      toPlace: 'LHR',
      toDate: '2018-04-27',
      fromPlace: 'EDI',
      fromDate: '2018-04-23'
  })
  .then((results) => {
    // TODO - a better format for displaying results to the client
    console.log('TODO: transform results for consumption by client');

    // USE THE CODE I DID IN THE FRONT END, AND CREATE A CLASS OUT OF IT
    res.json(results);
  })
  .catch(console.error);
});

app.listen(4000, () => {
  console.log('Node server listening on http://localhost:4000');
});
