## Example of Front End Architecture
- components, react, sass etc

## Design And Layout
- Example of custom css and sass

## Front End implementation

Example of React, ES6, ES7 classes
- `npm install` (once)

- `npm run client` to start the client development 
build at [http://localhost:3000](http://localhost:3000)


## API Node implementation
- Example of node, express implimentation, could be better but its a test.

- `APIKEY=<key> npm run server`
from the command line, it listens at
[http://localhost:4000](http://localhost:4000)

The API will return collections of different items:

* **Itineraries** - 
These are the container for your trips, 
tying together **Legs**, and **prices**. Prices are offered by an **agent** - an airline or travel agent.

* **Legs** - These are journeys (outbound, return) 
with **duration**, and **carriers**<sup>[1](#footnote1)</sup>. 
These contain one or more **Segments** and **stops**.

* **Segments** - Individual flight information 
with directionality.

A good structure to represent 
trip options would be hierarchical:

```
Itineraries
  Legs
    Segments
```

Your key will be rate-limited to ~5 queries per minute.
