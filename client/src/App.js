import React, { Component } from 'react';
import './App.scss';
import Header from './components/header';
import UiControls from './components/uicontrols';
import Listings from './components/listings';
import TopNav from './components/topnav';

/* eslint-disable */ // just to get get things done in time, because errors came up near the end,

class App extends Component {
  render() {
    return (
      <div className="App">
        <TopNav/>
        <Header/>
        <UiControls/>
        <Listings/>
      </div>
    );
  }
}
export default App;
