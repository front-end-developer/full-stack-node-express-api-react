/**
 * Created by WebEntra on 18/04/2018.
 */

import React from 'react';
import './style.scss';

class UiControls extends React.Component {

    constructor (props) {
        super(props);
        this.filterHandler = this.filterHandler.bind(this, event);
        this.sortHandler = this.sortHandler.bind(this, event);
        this.priceAlertsHandler = this.priceAlertsHandler.bind(this, event);
    }

    filterHandler(data, e) {
        console.log('filterHandler');
    }

    sortHandler(data, e) {
        console.log('sortHandler');
    }

    priceAlertsHandler(data, e) {
        console.log('priceAlertsHandler');
    }


    render () {
        return (
            <section className="ui-controls">
                <span onClick={this.filterHandler} className="filter">Filter</span>
                <span onClick={this.sortHandler} className="sort">Sort</span>
                <span onClick={this.priceAlertsHandler} className="price-alerts"><i className="bpk-icon"><img src="images/notifcation-temp-placeholder.jpg" alt="to" /></i>Price alerts</span>
            </section>
         )
    }
}
export default UiControls;