/**
 * Created by WebEntra on 18/04/2018.
 */
/* eslint-disable */ // just to get get things done in time, because errors came up near the end,
import React, {Component}  from 'react';
import moment from 'moment';
import './style.scss';

class Detail extends Component {

    constructor(props) {
        super(props);
        this.goToItinerary = this.goToItinerary.bind(this, event);
    }

    goToItinerary(data, e) {
        window.location.assign(this.props.link);
    }

    /**
     * TODO: work out the duration
     */
    render () {
        const {
            outboundDeparture,
            outboundArrival,
            outboundOrigin,
            outboundDestination,
            outboundDuration,
            outboundStops,
            outboundCarrier,
            inboundDeparture,
            inboundArrival,
            inboundOrigin,
            inboundDestination,
            inboundDuration,
            inboundStops,
            inboundCarrier,
            price,
            link
        } = this.props;
        const outboundNumberOfStops = (outboundStops.length == 0) ? 'Direct' : outboundStops.length;
        const inboundNumberOfStops = (inboundStops.length == 0) ? 'Direct' : inboundStops.length;
        return (
            <div className={`list-detail-component ${(outboundDeparture.length == 0) ? 'loading' : ''}`}>
                <div className="itinerary from">
                        <span className="logo">
                            <img src={outboundCarrier[0].ImageUrl} alt={outboundCarrier.Name} />
                        </span>
                    <span className="leg">
                            <span className="time">{moment(outboundDeparture).format('HH:mm')}</span>
                            <span className="location">{outboundDestination[0].Code}</span>
                        </span>
                    <span className="direction"><img src="images/temp-arrow-placeholder-should-be-a-font.jpg" alt="to" /></span>
                    <span className="leg">
                            <span className="time">{moment(outboundArrival).format('HH:mm')}</span>
                            <span className="location">{outboundOrigin[0].Code}</span>
                        </span>
                    <span className="steps-group">
                            <span className="duration">{outboundDuration}</span>
                            <span className="steps">{outboundNumberOfStops}</span>
                        </span>
                </div>
                <div className="itinerary to">
                        <span className="logo">
                            <img src={inboundCarrier[0].ImageUrl} alt={inboundCarrier.Name}  />
                        </span>
                    <span className="leg">
                            <span className="time">{moment(inboundDeparture).format('HH:mm')}</span>
                            <span className="location">{inboundDestination[0].Code}</span>
                        </span>
                    <span className="direction"><img src="images/temp-arrow-placeholder-should-be-a-font.jpg" alt="to" /></span>
                    <span className="leg">
                            <span className="time">{moment(inboundArrival).format('HH:mm')}</span>
                            <span className="location">{inboundOrigin[0].Code}</span>
                        </span>
                    <span className="steps-group">
                            <span className="duration">{inboundDuration}</span>
                            <span className="steps">{inboundNumberOfStops}</span>
                        </span>
                </div>
                <div className="itinerary-price">
                        <span className="price-record">
                            <span className="price">£{price}</span>
                            <span className="url">omegaflightstore.com</span>
                        </span>
                    <span className="call-to-action">
                            <button onClick={this.goToItinerary} type="button">Select</button>
                        </span>
                </div>
            </div>
         )
    }
}
Detail.defaultProps = {
    outboundDeparture: '',
    outboundArrival: '',
    outboundOrigin: '',
    outboundDestination: '',
    outboundDuration: '',
    outboundStops: '',
    outboundCarrier: '',
    inbounddDeparture: '',
    inboundArrival: '',
    inboundOrigin: '',
    inboundDestination: '',
    inboundDuration: '',
    inboundStops: '',
    inboundCarrier: '',
    price: '',
    link: ''
}
export default Detail;
