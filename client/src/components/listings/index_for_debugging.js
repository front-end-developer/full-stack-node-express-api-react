/**
 * Created by WebEntra on 18/04/2018.
 */
/* eslint-disable */
import React from 'react';
import Detail from './detail/index';
// import Store from '../../services/service';
import './style.scss';

//  TODO: create a DAO class for this
// this is a quick groud method
const ItinerariesDAO = function() {
    // set defaults
    const outboundLegs = [];
    const inboundLegs = [];
    const price = '';
    const link = '';
    outboundLegs[0] = {};
    inboundLegs[0] = {};
    outboundLegs[0].Departure = '';
    outboundLegs[0].Arrival = '';
    outboundLegs[0].DestinationStationPlaceCode = [
        {Code: ''}
    ];
    outboundLegs[0].OriginStationPlaceCode = [
        {Code: ''}
    ];
    outboundLegs[0].Duration = '';
    outboundLegs[0].Stops = '';
    outboundLegs[0].CarriersInformation = [
        {ImageUrl: '', Name: ''}
    ];
    inboundLegs[0].Departure = '';
    inboundLegs[0].Arrival = '';
    inboundLegs[0].DestinationStationPlaceCode = [
        {Code: ''}
    ];
    inboundLegs[0].OriginStationPlaceCode = [
        {Code: ''}
    ];
    inboundLegs[0].Duration = '';
    inboundLegs[0].Stops = '';
    inboundLegs[0].CarriersInformation = [
        {ImageUrl: '', Name: ''}
    ];
    return [{
        outboundLegs: outboundLegs,
        inboundLegs: inboundLegs,
        price: price,
        link: link
    }];
}

class List extends React.Component {

    constructor (prop) {
        super(prop);
        this.state = {
            'itinerariesSummary': null,
            'itinerariesCollection': [] // TODO: move to the service object
        };
        console.log( JSON.stringify(this.props, null, 7) );
    }

    postData() {
        /**
         * TODO: GET VALUES FROM A WEBFORM
         */
        const formData = {
            adults: 1,
            class: 'Economy',
            toPlace: 'LHR',
            toDate: '2018-04-27',
            fromPlace: 'EDI',
            fromDate: '2018-04-23'
        };

    }

    transData(itinerary, results) {
        const searchResults = {
            itineraries: itinerary,
            legs: results.Legs,
            segments: results.Segments,
            places: results.Places
        };
        console.log('searchResults: ', searchResults);


        // flightSummary
        const itinerariesSummary = {
            price:  itinerary.PricingOptions[0].Price, // [two decimal number]
            agent:  itinerary.PricingOptions[0].Agents[0], // 		[array]
            link:   itinerary.PricingOptions[0].DeeplinkUrl, //	[String]
            outboundLegId:  itinerary.OutboundLegId,
            inboundLegId:   itinerary.InboundLegId
        }

        const inboundLegs = results.Legs.filter((leg) => {
            return leg.Id === itinerariesSummary.inboundLegId;
        });

        const outboundLegs = results.Legs.filter((leg) => {
            return leg.Id === itinerariesSummary.outboundLegId;
        });

        /**
         * @description get the Aircraft carrier information based on
         * the first ID inside the inboundlegs & outboundlegs
         * carrier array (use the first carrier Id in each array only, I THINK),
         * TODO: and then add it to each
         */
        // TODO: use a better O notation algorithm
        for (let item in outboundLegs) {
            const carrier = results.Carriers.filter(function (carrier) {
                console.log(outboundLegs[item].Carriers[0], '===', carrier.Id);
                return carrier.Id === outboundLegs[item].Carriers[0];
            });
            outboundLegs[item].CarriersInformation = carrier;
        }

        // TODO: use a better O notation algorithm
        for (let item in inboundLegs) {
            const carrier = results.Carriers.filter(function (carrier) {
                console.log(inboundLegs[item].Carriers[0], '===', carrier.Id);
                return carrier.Id === inboundLegs[item].Carriers[0];
            });
            inboundLegs[item].CarriersInformation = carrier;
        }

        /**
         * TODO: perhaps I should not use only the first item in the
         * inboundLegs but I can fix it after the test
         *
         * @description not all Segments are marked as Outbound, so this may not be correct but, but Outbound might be standard for all inbound and outbound flights for Segments
         */
        if (inboundLegs.length) {
            for (let item in inboundLegs) {
                const inboundSegment = results.Segments.find((segment) => {
                    if (segment.FlightNumber === inboundLegs[item].FlightNumbers[0].FlightNumber
                        && segment.Carrier === inboundLegs[item].FlightNumbers[0].CarrierId
                        && segment.Id === inboundLegs[item].SegmentIds[0] // <-- TODO: verify why do we have an array of segment IDs here....
                    ) {
                        ;
                        console.log('FOUND');
                        console.log('1: ', inboundLegs[item].FlightNumbers[0].FlightNumber, '===', segment.FlightNumber);
                        console.log('2: ', inboundLegs[item].FlightNumbers[0].CarrierId, '===', segment.Carrier);
                        return segment;
                    }
                    // perhaps I should do something to send back {}
                });
                console.log('found: ', inboundSegment);
                delete inboundSegment.Directionality;
                inboundLegs[item].SegmentInformation = inboundSegment;
            }

            inboundLegs.forEach((leg) => {

                console.log('OriginStation: ', leg.OriginStation);

                console.log('DestinationStation: ', leg.DestinationStation);
                const DestinationStationPlaceCode = results.Places.filter((place) => {
                    return place.Id === leg.DestinationStation;
                });
                leg.DestinationStationPlaceCode = DestinationStationPlaceCode;
                console.log('DestinationStationPlaceCode: ', DestinationStationPlaceCode);
                const OriginStationPlaceCode = results.Places.filter((place) => {
                    return place.Id === leg.OriginStation;
                });
                leg.OriginStationPlaceCode = OriginStationPlaceCode;
                console.log('OriginStationPlaceCode: ', OriginStationPlaceCode);
            });
            itinerariesSummary.inboundLegs = inboundLegs;
        }


        if (outboundLegs.length) {
            for (let item in outboundLegs) {
                const outboundSegment = results.Segments.find((segment) => {
                    if (segment.FlightNumber === outboundLegs[item].FlightNumbers[0].FlightNumber
                        && segment.Carrier === outboundLegs[item].FlightNumbers[0].CarrierId
                        && segment.Id === outboundLegs[item].SegmentIds[0] // <-- TODO: verify why do we have an array of segment IDs here....
                    ) {

                        console.log('FOUND');
                        console.log('1: ', outboundLegs[item].FlightNumbers[0].FlightNumber, '===', segment.FlightNumber);
                        console.log('2: ', outboundLegs[item].FlightNumbers[0].CarrierId, '===', segment.Carrier);
                        return segment;
                    }
                    // perhaps I should do something to send back {}
                });
                console.log('found: ', outboundSegment);
                delete outboundSegment.Directionality; // <!-- Double check this, why does inbound does not exist on Segments????
                outboundLegs[item].SegmentInformation = outboundSegment;
            }

            outboundLegs.forEach((leg) => {
                const DestinationStationPlaceCode = results.Places.filter((place) => {
                    return place.Id === leg.DestinationStation;
                });
                leg.DestinationStationPlaceCode = DestinationStationPlaceCode;
                const OriginStationPlaceCode = results.Places.filter((place) => {
                    return place.Id === leg.OriginStation;
                });
                leg.OriginStationPlaceCode = OriginStationPlaceCode;
            });
            itinerariesSummary.outboundLegs = outboundLegs;
        }

        debugger;
       // this.setState({
            //'itinerariesSummary': itinerariesSummary
        //});

        var newItinerariesSummary = this.state.itinerariesCollection.slice();
        newItinerariesSummary.push(itinerariesSummary);
        this.setState({
            'itinerariesCollection': newItinerariesSummary
        });

        console.log(this.state.itinerariesCollection.length, ') Object-itinerariesSummary:', JSON.stringify(itinerariesSummary, null, 7));
    }

    componentDidMount() {
        // example api use
        // TODO put this call somewhere sensible
        // TODO send parameters to server - check out `server/src/api/server.js`
        console.log('fetching results from server...');
debugger;

        /**
         * TODO:
         * use Store instead and use Observables.subscribe
         */
        fetch('http://localhost:4000/api/search')
            .then((response) => {
                return response.json();
            })
            .then((results) => {
                debugger;
                console.log('TODO: something with these results:');
                console.log(results);

                const maxItineraries = 5;
                const itinerariesList = [];
                for (let i = 0; i < maxItineraries; i++) {
                    // itinerariesList.push(results.Itineraries);
                    this.transData(results.Itineraries[i], results);
                }
            })
            .catch(console.error);
    }

    render () {
        let legs = this.state.itinerariesCollection.length > 0 ? this.state.itinerariesCollection : ItinerariesDAO();

        console.log('LEGs.length: ', legs.length);
        console.log('LEG: ', legs);

        debugger;

        return (
            <section className="list-component">
                {
                    legs.map((leg, index)=>(
                        <Detail
                            outboundDeparture={leg.outboundLegs[0].Departure}
                            outboundArrival={leg.outboundLegs[0].Arrival}
                            outboundDestination={leg.outboundLegs[0].DestinationStationPlaceCode}
                            outboundOrigin={leg.outboundLegs[0].OriginStationPlaceCode}
                            outboundDuration={leg.outboundLegs[0].Duration}
                            outboundStops={leg.outboundLegs[0].Stops}
                            outboundCarrier={leg.outboundLegs[0].CarriersInformation}
                            inboundDeparture={leg.inboundLegs[0].Departure}
                            inboundArrival={leg.inboundLegs[0].Arrival}
                            inboundDestination={leg.inboundLegs[0].DestinationStationPlaceCode}
                            inboundOrigin={leg.inboundLegs[0].OriginStationPlaceCode}
                            inboundDuration={leg.inboundLegs[0].Duration}
                            inboundStops={leg.inboundLegs[0].Stops}
                            inboundCarrier={leg.inboundLegs[0].CarriersInformation}
                            price={leg.price}
                            link={leg.link}
                        />
                    ))
                }

            </section>
        )
    }
}
export default List;
