/**
 * Created by WebEntra on 18/04/2018.
 */
/* eslint-disable */
import React from 'react';
import './style.scss';

class Header extends React.Component {

    constructor (props) {
        super(props)
    }

    render () {
        return (
            <section className="flight-information">
                EDI <span className="arrow"><img src="images/temp-BIG-arrow-placeholder-should-be-a-font.jpg" alt="to" /></span> LON
                <span className="person-class">2 travellers, economy</span>
            </section>
         )
    }
}
export default Header;
