# Implementation:

### Q) What libraries did you add to the frontend? What are they used for?
font-awesome-sass-loader to load icons.
but i never used it becasue your I think you are using an older version of webpack.
for node:
colors, makes my life easier during debugging.


### Q) What is the command to start the server?

(Default) `APIKEY=<key> npm run server`

---

# General:

### Q) How long, in hours, did you spend on the test?
1 day and a half. 

### Q) If you had more time, what further improvements or new features would you add?

make the header show values dynamically...i hard coded it so I could finish the front-end styling

perhaps use a node package to encrypte/decrypt the API key

add auto-pagination for the top and bottom

add the agent details to the their web address will appear dynamically.

add the code for the Observables to the services/services.js and use that as like Singleton.

update the front end architecture,

use Observables and Observables.subscribe() instead of promises,

add Typescript typing

update change the backend architecture,

use RSJX observables instead of promises
and seperate out the npm packages for front end and backend,

I would have used webpack three

add more Rules in the linting, and babel.rc

more work in the sass architecture

learn & use Skyscanner styling 

I could have used more the the lodash underscore functions to filter and sort data, but I was just coded it up quickly in core vanilla JS.

used glph fonts intead of images,

wire up the sass variable better to work in all sub components

seperate out the templates inside the react render function into there own jsx files, and them automate some minification or something on them.

include more bells and whistles.... ;)

finish everything including the search criteria,

add tabs so users can search for hotels, and car hire etc

include google click analytics

finish the nav menu

finish everything the entire searches etc, 

stick it on one of my websites :)






### Q) Which parts are you most proud of? And why?
Front end, because it is easier and straight forward,
backend I enjoy also...I wish I had more time, I will continue this after I get my next offer.


### Q) Which parts did you spend the most time with? What did you find most difficult?
the styling bit, but mostly just working out what data relates to what data. 

Lots of promises all over the place, I prefer to use Obervables.subscribe, its far more optimised to use that.

### Q) How did you find the test overall? If you have any suggestions on how we can improve the test or our API, we'd love to hear them.

It was fun, I never had time to read all your api docs in detail, but you want to allow pagination or limit by calls on your api.

seams to be some duplicate date in the results from the skyscanner api, Reduce payload by removeing duplicates.

I spent more time, trying to work out what is in the data and where things are, and the relationships then anything else.

